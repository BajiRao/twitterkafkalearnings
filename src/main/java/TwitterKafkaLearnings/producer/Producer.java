package TwitterKafkaLearnings.producer;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import com.google.common.collect.Lists;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.Hosts;
import com.twitter.hbc.core.HttpHosts;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;

import TwitterKafkaLearnings.utils.AuthDetails;
import lombok.extern.slf4j.Slf4j;

@Slf4j
class Producer {

	public static void main(String[] args) throws IOException {
		new Producer().run();
	}

	public void run() throws IOException {

		BlockingQueue<String> msgQueue = new LinkedBlockingQueue<>(1000);

		// create a twitter client
		Client client = createTwitterClient(msgQueue);

		// create a kafka producer
		KafkaProducer<String, String> producer = createKafkaProducer();

		// loop to send tweets to kafka
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			log.info("stopping the Application...");
			client.stop();
			producer.close();
			log.info("Application Stopped...");
		}));

		while (!client.isDone()) {
			String msg = null;
			try {
				msg = msgQueue.poll(5, TimeUnit.SECONDS);
			} catch (Exception e) {
				e.printStackTrace();
				client.stop();
			}
			if (msg != null) {
				log.info(msg);
				producer.send(new ProducerRecord<String, String>("twitter_tweets", null, msg), new Callback() {

					@Override
					public void onCompletion(RecordMetadata metadata, Exception exception) {
						if (exception != null) {
							log.error("Exception has occurred ", exception);
						}
					}
				});
			}
			producer.flush();
		}
	}

	public Client createTwitterClient(BlockingQueue<String> msgQueue) {
		Hosts hosebirdHosts = new HttpHosts(Constants.STREAM_HOST);
		StatusesFilterEndpoint hosebirdEndpoint = new StatusesFilterEndpoint();

		List<Long> followings = Lists.newArrayList(1234L, 566788L);
		List<String> terms = Lists.newArrayList("twitter", "api");
		hosebirdEndpoint.followings(followings);
		hosebirdEndpoint.trackTerms(terms);

		Authentication authentication = new OAuth1(AuthDetails.API_KEY, AuthDetails.API_SECRET,
				AuthDetails.ACCESS_TOKEN, AuthDetails.ACCESS_TOKEN_SECRET);

		ClientBuilder builder = new ClientBuilder().name("KafkaLearnings") // optional: mainly for the logs
				.hosts(hosebirdHosts).authentication(authentication).endpoint(hosebirdEndpoint)
				.processor(new StringDelimitedProcessor(msgQueue));

		Client hbc = builder.build();
		// Attempts to establish a connection.
		hbc.connect();
		return hbc;
	}

	public KafkaProducer<String, String> createKafkaProducer() throws IOException {
		Properties producerProperties = new Properties();
		try (InputStream producerConfigFile = getClass().getResourceAsStream("/producer-config.properties")) {
			producerProperties.load(producerConfigFile);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return new KafkaProducer<>(producerProperties);
	}

}
