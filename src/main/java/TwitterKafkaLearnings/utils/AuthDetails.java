 package TwitterKafkaLearnings.utils;

public class AuthDetails{
    public static final String API_KEY="";
    public static final String API_SECRET="";
    public static final String ACCESS_TOKEN="";
    public static final String ACCESS_TOKEN_SECRET="";
    
    private AuthDetails() {}
}